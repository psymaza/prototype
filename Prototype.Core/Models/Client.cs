using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Prototype.Core.Interfaces;

namespace Prototype.Core.Models
{
    public class Client : Person, IMyCloneable<Client>
    {
        public IList<string> ServiceRendered { get; set; }

        public Client()
        {
            ServiceRendered = new List<string>();
        }

        public override void Introduce()
        {
            Console.WriteLine("Hello, I'm {0}", Name);
        }

        public override Client Clone()
            => Copy();

        public Client Copy()
            => new()
            {
                Name = Name,
                Age = Age,
                ServiceRendered = ServiceRendered.Select(e => e).ToList()
            };

        public override bool Equals(object? o)
        {
            if (o is Client client and not null)
            {
                return client.Name == Name &&
                       client.Age == Age &&
                       client.ServiceRendered.SequenceEqual(ServiceRendered);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Age, ServiceRendered);
        }
    }
}