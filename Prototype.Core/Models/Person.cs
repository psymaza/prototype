using System;
using Prototype.Core.Interfaces;

namespace Prototype.Core.Models
{
    public abstract class Person : ICloneable
    {
        public string Name { get; set; }
        public byte Age { get; set; }

        public abstract void Introduce();

        public abstract object Clone();
    }
}