#nullable enable
using System;
using Prototype.Core.Interfaces;

namespace Prototype.Core.Models
{
    public class Employee : Person, IMyCloneable<Employee>
    {
        public string Position { get; set; }
        public decimal Salary { get; set; }

        public override void Introduce()
        {
            Console.WriteLine("Hello, I'm {0} {1}", Position, Name);
        }

        public override Employee Clone()
            => Copy();

        public Employee Copy()
            => new()
            {
                Name = Name,
                Age = Age,
                Position = Position,
                Salary = Salary
            };

        public override bool Equals(object? o)
        {
            if (o is Employee employee and not null)
            {
                return employee.Name == Name &&
                       employee.Age == Age &&
                       employee.Position == Position &&
                       employee.Salary == Salary;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Age, Position, Salary);
        }
    }
}