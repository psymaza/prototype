namespace Prototype.Core.Interfaces
{
    public interface IMyCloneable<T>
    {
        T Copy();
    }
}