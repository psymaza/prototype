using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using Prototype.Core.Models;

namespace Prototype.Tests
{
    public class Tests
    {
        private Client _testClient;
        private Employee _testEmployee;

        [SetUp]
        public void Setup()
        {
            _testClient = new()
            {
                Name = "Вася",
                Age = 34,
                ServiceRendered =
                {
                    "Мойка полов",
                    "Покраска стен"
                }
            };

            _testEmployee = new()
            {
                Name = "Петя",
                Age = 43,
                Position = "Марял",
                Salary = 50000
            };
        }

        [Test]
        public void ClientCopyFieldsEquals()
        {
            var clientCopy = _testClient.Copy();
            Assert.AreEqual(clientCopy, _testClient);
        }

        [Test]
        public void ClientCopyReferenceEquals()
        {
            var clientCopy = _testClient.Copy();
            Assert.That(clientCopy, Is.Not.SameAs(_testClient));
        }

        [Test]
        public void ClientCloneTest()
        {
            var clientCopy = _testClient.Clone();
            Assert.AreEqual(clientCopy, _testClient);
            Assert.That(clientCopy, Is.Not.SameAs(_testClient));
        }

        [Test]
        public void EmployeeCopyFieldsEquals()
        {
            var employeeCopy = _testEmployee.Copy();
            Assert.AreEqual(employeeCopy, _testEmployee);
        }

        [Test]
        public void EmployeeCopyReferenceEquals()
        {
            var clientCopy = _testEmployee.Copy();
            Assert.That(clientCopy, Is.Not.SameAs(_testEmployee));
        }

        [Test]
        public void EmployeeCloneTest()
        {
            var clientCopy = _testEmployee.Clone();
            Assert.AreEqual(clientCopy, _testEmployee);
            Assert.That(clientCopy, Is.Not.SameAs(_testEmployee));
        }
    }
}