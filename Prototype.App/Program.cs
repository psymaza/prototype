﻿using System;
using System.Net.Http;
using Prototype.Core.Models;

namespace Prototype.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var clientVasya = new Client
            {
                Name = "Вася",
                Age = 33,
                ServiceRendered =
                {
                    "Мойка полов",
                    "Покраска стен"
                }
            };

            var clientPeter = clientVasya.Copy();

            clientVasya.Introduce();
            clientPeter.Introduce();

            Console.WriteLine("У клиентов совпадают значения полей, но разные ссылки: {0}", clientVasya.Equals(clientPeter) && !ReferenceEquals(clientVasya, clientPeter));
        }
    }
}